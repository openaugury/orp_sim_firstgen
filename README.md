# OurRagingPlanet - 1st Gen Simulation Toolkit

## for Python3

### Handler

    _phenomena = { # Dictonary object for storing risks
        'surge-elevation': SurgeRisk,
        'earthquake-shibata-study': QuakeRisk,
        'volcano-ash-deposition': VolcanoConcentricRisk,
        'volcano-lava': VolcanoLavaRisk,
        'concentric': ConcentricRisk
    }

    def phenomena(which):  # Function for returning a risk based on a choice
        print(which) # Print out choice to screen?
        print(_phenomena) # Print out dict object to screen?
        if which in _phenomena: # If the choice is in the dict do this
            return _phenomena[which]() # Return dict that applied to the choice  creates new instance
        return None # If the choice is not in the dict do this

