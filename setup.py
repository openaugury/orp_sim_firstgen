import os
from setuptools import setup

setup(
    name='orp_sim_firstgen',
    packages=[
        'orp_sim_firstgen'
    ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    package_data={
        'config': ['config/config.json'],
        'orp_sim_firstgen': ['data/50m.csv']
    }
)
