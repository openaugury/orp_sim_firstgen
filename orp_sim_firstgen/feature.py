class Feature:
    def __init__(self, id, location):
        self._id = id
        self._location = location
        self._current_health = {}
        self._height = 0

    def getLatLng(self):
        return self._location

    def updateHealth(self, t, f):
        self._current_health[t] = 100. * (1 - f(*self._location)[0][0])

    def setHeight(self, z):
        self._height = z

    def healthDict(self):
        return {
            'h': sorted([(t, h) for t, h in self._current_health.items()])
        }
