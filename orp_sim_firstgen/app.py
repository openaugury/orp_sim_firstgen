#This class is used for creating an object with type app
from flask import Flask, redirect
from flask_restful import Resource, Api, reqparse
import os
import numpy as N
import json
import time
from minio import Minio
from minio.error import ResponseError as MinioResponseError
from .resources import Result
from .result_service import ResultService
from function import handler


def create_app(): # This function is used for creating an app object. Will return an object of type app.
    app = Flask(__name__) # Creating application object, standard for setting up Flask
    api = Api(app) # Setting up the Flask API

    #redis_args = {   # Dictionary object that contains the host, port, timeout for the socket and the database. Used for connection
    #    'host': os.environ['REDIS_HOST'],  #Host for connection
    #    'port': os.environ['REDIS_PORT'],  #Port for connection
    #    'socket_timeout': 1,  #Time before timeout for socket
    #    'db': 0  #Database
    #}
    #if 'REDIS_PASS' in os.environ:  #If the password for Redis is in the os.environ array, do this.
    #    redis_args['password'] = os.environ['REDIS_PASS']  #password from redis_args becomes password from os.environ

    #_redis = redis.StrictRedis(**redis_args)  # Keyword expansion for dict object
    if all([k in os.environ for k in ('minio_endpoint', 'minio_key', 'minio_secret', 'minio_bucket')]):
        mo_config = {
            'endpoint': os.environ['minio_endpoint'],
            'key': os.environ['minio_key'],
            'secret': os.environ['minio_secret'],
            'region': 'us-east-1',
            'bucket': os.environ['minio_bucket']
        }

        mo = Minio(
            mo_config['endpoint'],
            access_key=mo_config['key'],
            secret_key=mo_config['secret'],
            region=mo_config['region'],
            secure=True
        )
        mo_bucket = mo_config['bucket']
    else:
        mo = None
        mo_bucket = None

    api.add_resource(Result, '/<result_id>', resource_class_kwargs={'handler': handler.handle, 'mo': (mo, mo_bucket)})  #Add resource to the API?

    # For local experimentation
    app.add_url_rule(
        '/function/<function>/<result_id>',
        view_func=lambda function, result_id: redirect('/{}'.format(result_id), code=307),
        methods=['HEAD', 'GET', 'POST', 'OPTIONS']
    )

    ResultService.preload(handler.risk)   #Call load function from result_service object
    app.logger.error("Preload done.")

    return app      #return app object
