from abc import ABC, abstractmethod


class Risk(ABC): # limited model showing of distance w concentric damage
    @classmethod
    @abstractmethod
    def preload(cls):
        """Indicate any standard data that should be preloaded (currently only elevations).
        Available tokens: 'heights'
        :return set: Set of tokens requesting preload of standard data."""
        ...

    @abstractmethod
    def load(self, logger, center, z, **parameters):  # Load function: loads risk range,logger,epicentre,and parameters
        ...

    def advance_time(self, time):  # Function for advancing the time
        self._time = time # The time that's passed in through the parameter for this function is assigned to self

    # Are x and y definitely in SI?
    @abstractmethod
    def calculate(self, x, z, i, j): # Calcuating the risk
        """Calculate the risk.
        :return float: Level of risk [0,1]."""
        return 0 # Take one away from inital health value (e.g. this return 0 indicates no risk == full health)

    def finish(self): #  Finish function
        pass
