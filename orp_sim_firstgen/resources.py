from flask import Flask
from flask_restful import Resource, reqparse
from flask import current_app
import json
import fs as filesystem
import io
from minio.error import ResponseError as MinioResponseError

from .feature import Feature

class Result(Resource):
    step = 3000

    def __init__(self, handler, mo): #Constructor
        self._handler = handler
        self._mo_client, self._mo_bucket = mo

    def post(self, result_id): # Requests
        parser = reqparse.RequestParser()
        parser.add_argument('window')

        parser.add_argument('time')

        parser.add_argument('phenomenon')
        parser.add_argument('definition')
        parser.add_argument('center')

        parser.add_argument('version')
        parser.add_argument('begin')
        parser.add_argument('end')

        parser.add_argument('features')
        parser.add_argument('minio_key')
        parser.add_argument('location')
        parser.add_argument('root')
        args = parser.parse_args()

        window = json.loads(args['window'])
        center = json.loads(args['center'])
        feature_sets = {}
        current_app.logger.info(args)

        if args['location']:
            current_app.logger.error('Opening location: ' + args['root'])
            features_root = filesystem.open_fs(args['root'])
            settings_file = features_root.open(args['settings_location'])
            settings = settings_file.read()
            features_file = features_root.open(args['features_location'])
            features = features_file.read()
        elif args['minio_key'] and self._mo_client:
            current_app.logger.error('Opening minio: ' + args['minio_key'])
            key = args['minio_key']

            try:
                data_object = self._mo_client.get_object(self._mo_bucket, 'simulations/{}.json'.format(key))
                stream_req = io.BytesIO()
                for d in data_object.stream(32 * 1024):
                    stream_req.write(d)
            except MinioResponseError as err:
                current_app.logger.error(err)

            features = stream_req.getvalue().decode('utf-8')
        else:
            current_app.logger.error('Opening features from URL')
            key = None
            features = args['features']

        for feature_set in json.loads(features):
            feature_sets[feature_set['set_id']] = []
            for fs in feature_set['chunk']:
                coordinates = fs['location']['coordinates']
                coordinates.reverse()
                feature_sets[feature_set['set_id']].append(Feature(fs['feature_id'], coordinates))

        if args['version'] != '2':
            times = [int(args['time'])]
        else:
            times = range(0, int(args['end']) - int(args['begin']), self.step)

        if args['definition']:
            definition = json.loads(args['definition'])
            risk_parameters = definition['parameters']
            if not risk_parameters:
                risk_parameters = {}
        else:
            risk_parameters = {}

        feature_sets, calc_results = self._handler(times, risk_parameters, feature_sets, center[0], center[1], *window)
        results = []
        for result in calc_results:
            time, points, f, grid = result

            points = {"%d,%d" % k: v for k, v in points.items()}

            if args['begin']:
                time += int(args['begin'])
            results.append({'time': time, 'points': points, 'grid': grid})

        if args['version'] != '2':
            results = results[0]

        feature_arcs = {str(k): [l.healthDict() for l in v] for k, v in feature_sets.items()}

        current_app.logger.error('Finished')
        current_app.logger.error({'results': len(results), 'feature_arcs': len(feature_arcs)})

        if key and args['minio_key'] and self._mo_client:
            try:
                data_object = self._mo_client.remove_object(self._mo_bucket, 'simulations/{}.json'.format(key))
            except MinioResponseError as err:
                current_app.logger.error(err)

            serial_res = bytes(json.dumps(results), encoding='utf-8')
            stream_res = io.BytesIO(serial_res)

            key_result = '{}_result'.format(key)
            try:
                self._mo_client.put_object(
                    self._mo_bucket,
                    'simulations/{}.json'.format(key_result),
                    stream_res,
                    len(serial_res)
                )
            except MinioResponseError as err:
                current_app.logger.error(err)

            serial_res = bytes(json.dumps(feature_arcs), encoding='utf-8')
            stream_res = io.BytesIO(serial_res)

            key_feature_arcs = '{}_feature_arcs'.format(key)
            try:
                self._mo_client.put_object(
                    self._mo_bucket,
                    'simulations/{}.json'.format(key_feature_arcs),
                    stream_res,
                    len(serial_res)
                )
            except MinioResponseError as err:
                current_app.logger.error(err)

            return {'results': key_result, 'feature_arcs': key_feature_arcs}
        else:
            return {'results': results, 'feature_arcs': feature_arcs}
