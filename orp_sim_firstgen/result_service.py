import os
import logging
import numpy as N
import time
from flask import current_app

from scipy import interpolate

from .utils import get_converter
from .plot import get_cached_50m, REGION

EPS = 1e-2

class ResultService: # This class will return results
    _cached_heights = None

    @classmethod
    def prefetch(cls, risk):
        datasets = {
            'test-1': 'dem/test-1.csv',
            'ni-1': 'dem/ni-1.csv',
            'sa-1': 'dem/sa-1.csv',
            'sa-2': 'dem/sa-2.csv',
            'tv-1': 'dem/tv-1-new.tif'
        }
        prefetch = {}
        cache_file = os.path.join('/tmp', datasets[REGION])
        if os.path.exists(cache_file):
            logging.error('Found in cache : %s', REGION)
            cls._cached_heights = get_cached_50m(cache_file)
        elif REGION:
            prefetch[datasets[REGION]] = cls.set_dem

        if hasattr(risk, 'prefetch'):
            prefetch.update(risk.prefetch())
        logging.error(REGION)
        return prefetch

    @classmethod
    def set_dem(cls, dem_stream):
        try:
            os.makedirs('/tmp/dem', exist_ok=True)
            cache_file = os.path.join('/tmp', 'dem', f'{REGION}.csv')
            with open(cache_file, 'w') as f:
                values = dem_stream.getvalue().decode('utf-8')
                f.write(values)
            dem_stream = cache_file
            logging.error('Cached : %s', REGION)
        except OSError as exc:
            logging.error('Could not cache : %s %s', REGION, str(exc))

        cls._cached_heights = get_cached_50m(dem_stream)

    @classmethod
    def preload(cls, risk):
        preload = risk.preload()

        print('preload2')
        if 'cached-heights' in preload:
            try:
                cls._cached_heights = preload['cached-heights']()
            except TypeError:
                pass

        if cls._cached_heights is None:
            cls._cached_heights = get_cached_50m()  # Set cached_heights to data from a file

    def retrieve(self, risk, times, risk_parameters, feature_sets, cla, clo, la1, lo1, la2, lo2): #
        converter = get_converter()()

        xx, yy, c = self._cached_heights
        xc = N.array(sorted(set(xx.flatten()))) # flattening into 2D array
        yc = N.array(sorted(set(yy.flatten())))
        x1, y1 = converter(la1, lo1, inverse=False)
        x2, y2 = converter(la2, lo2, inverse=False)

        inside = (x1 < xx) & (y1 < yy) & (x2 > xx) & (y2 > yy)
        xx = xx[inside]
        yy = yy[inside]
        w = c[inside]

        xc = xc[(x1 < xc) & (x2 > xc)]
        yc = yc[(y1 < yc) & (y2 > yc)]

        # Replace with RectBivariateSpline
        #f = interpolate.RectBivariateSpline(yc, xc, z)
        # Should f be called?
        try:
            f = interpolate.interp2d(xx.flatten(), yy.flatten(), w.flatten())
        except:
            f = lambda a, b: [0]
        for feature_set in feature_sets.values():
            for feature in feature_set:
                feature.setHeight(f(*converter(inverse=False, *feature.getLatLng()))[0])

        x1 = max(x1, xx.min())
        x2 = min(x2, xx.max())
        y1 = max(y1, yy.min())
        y2 = min(y2, yy.max())
        grid = {
            'northWest': converter(x1, y2),
            'southEast': converter(x2, y1),
            'divisionsLat': len(yc),
            'divisionsLon': len(xc)
        }
        xcl = sorted(list(xc))
        ycl = sorted(list(yc))

        if len(w):
            elevations = [(i, j, converter(xcl[i], ycl[j]), w[i + j * len(xcl)]) for i in range(len(xcl)) for j in range(len(ycl))]
        else:
            elevations = []

        risk.load(current_app.logger, (cla, clo), f(*converter(cla, clo, inverse=False))[0], heights=(xx, yy, w), **risk_parameters)

        results = []
        for t in times:
            current_app.logger.error('ELE: %d', len(elevations))
            risk.advance_time(t)

            if len(elevations):
                statistics = {}
                wrisk = N.array([risk.calculate(x, z, i, j) for i, j, x, z in elevations]).reshape((len(xcl), len(ycl)))
                current_app.logger.error(' - wrisk')
                frisk = interpolate.RectBivariateSpline(xcl, ycl, wrisk)
                current_app.logger.error(' - frisk')
                points = {(len(xcl) - i - 1, len(ycl) - j - 1): wrisk[i, j] for i, j, x, z in elevations if wrisk[i, j] > EPS}
                statistics['count-0.5'] = len([pt for pt in points.values() if pt > 0.5])
                current_app.logger.error(' - points [%d > 0.5]', statistics['count-0.5'])
                for feature_set in feature_sets.values():
                    for feature in feature_set:
                        current_app.logger.error(f'{feature._id} {feature._location}')
                        feature.updateHealth(t, lambda la, lo: frisk(*converter(la, lo, inverse=False)))
                current_app.logger.error(' - features')

                def test(x, y):
                    return risk.calculate(x, y, f(*converter(y, x, inverse=False))[0])

                results.append([t, points, lambda x, y: test(x, y), grid, statistics])
            else:
                results.append([t, {}, lambda x, y: 0., grid, {}])

        risk.finish()

        return feature_sets, results
