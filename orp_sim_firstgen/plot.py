import numpy as N
import logging
from osgeo import gdal
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from .utils import get_pre_converter
import matplotlib.cm as cm
from scipy import interpolate
from flask import current_app
import os

#xc, yc, xx, yy, c = N.load('cache/heights.npz', encoding='bytes')
REGION = os.environ.get('DATA_REGION', '')

HEIGHT_MAPS = {
    'ni-1': 'ni-1.csv', # NI
    'sa-1': 'sa-1.csv', # Riyadh
    'sa-2': 'sa-2.csv', # Dammam
    'tv-1': 'tv-1-new.tif' # Tomorrowville
}

def get_cached_50m_tiff(path):
    path_heights = f'/tmp/heights-{REGION}.npz'

    if True or not os.path.exists(path_heights):
        logging.error('1')
        orig_tiff = gdal.Open(path)
        info = [orig_tiff.GetProjection(),

                # Dimensions
                orig_tiff.RasterXSize,
                orig_tiff.RasterYSize,

                # Number of bands
                orig_tiff.RasterCount,
                orig_tiff.GetGeoTransform(),

                # Metadata for the dem dataset
                orig_tiff.GetMetadata()]
        orig_tiff = None
        logging.error('ORIG: ' + str(info))
        options = gdal.WarpOptions(options=['tr'], xRes=0.0003, yRes=0.0003, dstSRS='EPSG:4326', dstNodata=-9999, resampleAlg=gdal.GRA_NearestNeighbour)
        tiff = gdal.Warp(f'{path}.warp.tif', path, options=options)
        tiff = None
        logging.error('2')
        dem = gdal.Open(f'{path}.warp.tif')
        logging.error('3')
        arr = dem.ReadAsArray()
        logging.error('4')
        info = [dem.GetProjection(),
                arr.max(),
                arr.min(),

                # Dimensions
                dem.RasterXSize,
                dem.RasterYSize,

                # Number of bands
                dem.RasterCount,
                dem.GetGeoTransform(),

                # Metadata for the dem dataset
                dem.GetMetadata()]
        logging.error(str(info))
        logging.error(str(arr))
        #converter = get_pre_converter()()
        trans = dem.GetGeoTransform()
        min_xc = trans[0]
        max_yc = trans[3]
        max_xc = trans[0] + dem.RasterXSize * trans[1]
        min_yc = trans[3] + dem.RasterYSize * trans[5]
        #xc, yc, h = zip(*arr)
        #xi = N.arange(min_xc, max_xc, (max_xc - min_xc) / 3000)
        #yi = N.arange(min_yc, max_yc, (max_yc - min_yc) / 3000)

        ##heights = {(x[0], x[1]): 0 for x in zip(xx.flatten(), yy.flatten())}
        ## Note these are integer northings/eastings
        ##for x in arr:
        ##    c = (x[0], x[1])
        ##    if c not in heights:
        ##        app.logger.error(c)
        ##    heights[c] = x[2] #RMV

        xx, yy = N.meshgrid(
            N.linspace(min_xc, max_xc, dem.RasterXSize),
            N.linspace(min_yc, max_yc, dem.RasterYSize)
        )
        zz = arr
        #xc = xc.flatten()
        #yc = yc.flatten()
        #h = arr.T.flatten()
        logging.error((arr.shape, xx.shape, yy.shape, zz.shape, zz.min(), zz.max(), zz.mean()))
        #zz = interpolate.griddata((xc, yc), N.array(h), (xx, yy), method='nearest')
        #f = interpolate.interp2d(xi, yi, zz.T.flatten())

        #@N.vectorize
        #def get_height(x, y):
        #    return heights[(x, y)]

        cached_heights = N.array([xx, yy, zz.reshape(xx.shape)])
        llsw = (min_xc, min_yc)
        llse = (max_xc, min_yc)
        llnw = (min_xc, max_yc)
        llne = (max_xc, max_yc)
        llbb = [
            [min(llsw[0], llnw[0]), min(llse[1], llsw[1])],
            [max(llse[0], llne[0]), max(llne[1], llnw[1])],
        ]
        xi2 = N.arange(llbb[0][0], llbb[1][0], (llbb[1][0] - llbb[0][0]) / 1000)
        yi2 = N.arange(llbb[0][1], llbb[1][1], (llbb[1][1] - llbb[0][1]) / 1000)
        xx2, yy2 = N.meshgrid(xi2, yi2)
        xy = zip(xx2.flatten(), yy2.flatten())

        h2 = N.zeros(xx2.size)
        #for ix, c in enumerate(xy):
        #    h2[ix] = f(*converter(c[1], c[0], inverse=False))
        #    if ix % 10000 == 0:
        #        print(ix, '/', h2.size, c, h2[ix])
        logging.error("Dumped heights %d %d %d", len(xx), len(yy), len(zz))
        cached_heights = N.array([xx, yy, zz.reshape(xx.shape)])
        cached_heights.dump(path_heights)
        arr = N.savetxt('numpy.csv', [xx.flatten(), yy.flatten(), zz.flatten()])
    else:
        cached_heights = N.load(path_heights, allow_pickle=True)

    return cached_heights

def get_cached_50m(stream=None):
    if stream is None:
        path_50m = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'data',
            HEIGHT_MAPS[REGION]
        )
        with open(path_50m, 'r') as pathfile:
            return get_cached_50m(pathfile)
    elif isinstance(stream, str):
        if stream.endswith('tiff') or stream.endswith('tif'):
            return get_cached_50m_tiff(stream)
        with open(stream, 'r') as pathfile:
            return get_cached_50m(pathfile)

    path_heights = f'/tmp/heights-{REGION}.npz'

    if not os.path.exists(path_heights):
        converter = get_pre_converter()()
        logging.info(f"Loading heights ({REGION})")
        arr = N.loadtxt(stream, skiprows=1, delimiter=',')
        xc, yc, h = zip(*arr)
        xi = N.arange(min(xc), max(xc), (max(xc) - min(xc)) / 3000)
        yi = N.arange(min(yc), max(yc), (max(yc) - min(yc)) / 3000)
        xx, yy = N.meshgrid(xi, yi)

        #heights = {(x[0], x[1]): 0 for x in zip(xx.flatten(), yy.flatten())}
        # Note these are integer northings/eastings
        #for x in arr:
        #    c = (x[0], x[1])
        #    if c not in heights:
        #        app.logger.error(c)
        #    heights[c] = x[2] #RMV

        zz = interpolate.griddata((xc, yc), N.array(h), (xx, yy), method='nearest')
        f = interpolate.interp2d(xi, yi, zz.T.flatten())

        #@N.vectorize
        #def get_height(x, y):
        #    return heights[(x, y)]

        cached_heights = N.array([xx, yy, zz])
        llsw = converter(min(xc), min(yc))
        llse = converter(max(xc), min(yc))
        llnw = converter(min(xc), max(yc))
        llne = converter(max(xc), max(yc))
        llbb = [
            [min(llsw[1], llnw[1]), min(llse[0], llsw[0])],
            [max(llse[1], llne[1]), max(llne[0], llnw[0])],
        ]
        xi2 = N.arange(llbb[0][0], llbb[1][0], (llbb[1][0] - llbb[0][0]) / 1000)
        yi2 = N.arange(llbb[0][1], llbb[1][1], (llbb[1][1] - llbb[0][1]) / 1000)
        xx2, yy2 = N.meshgrid(xi2, yi2)
        xy = zip(xx2.flatten(), yy2.flatten())

        h2 = N.zeros(xx2.size)
        for ix, c in enumerate(xy):
            h2[ix] = f(*converter(c[1], c[0], inverse=False))
            if ix % 10000 == 0:
                print(ix, '/', h2.size, c, h2[ix])
        logging.info("Dumped heights")
        cached_heights = N.array([xx2, yy2, h2.T.reshape(xx2.shape)])
        cached_heights.dump(path_heights)
    else:
        cached_heights = N.load(path_heights, allow_pickle=True)

    return cached_heights

def write_raster(arr):
    la1, lo1 = [54.12455935451281, -6.542187013899322]
    la2, lo2 = [58.29547366514143, -5.739056407396573]

    xx, yy, c = arr
    xc = N.array(sorted(set(xx.flatten()))) # flattening into 2D array
    yc = N.array(sorted(set(yy.flatten())))
    x1, y1 = lo1, la1
    x2, y2 = lo2, la2

    inside = (x1 < xx) & (y1 < yy) & (x2 > xx) & (y2 > yy)
    #xx = xx[inside]
    #yy = yy[inside]
    #w = c[inside]
    w = c.flatten()

    #xc = xc[(x1 < xc) & (x2 > xc)]
    #yc = yc[(y1 < yc) & (y2 > yc)]

    # z1 = f(*converter(la1, lo1, inverse=False))[0]
    # z2 = f(*converter(lo1, lo2, inverse=False))[0]
    xcl = sorted(list(xc))
    ycl = sorted(list(yc))
    x1 = xcl[0]
    x2 = xcl[-1]
    y1 = ycl[0]
    y2 = ycl[-1]
    grid = {
        'northWest': (y2, x1),
        'southEast': (y1, x2),
        'divisionsLat': len(yc),
        'divisionsLon': len(xc)
    }
    print(w.shape)
    elevations = [(i, j, (xcl[i], ycl[j]), w[i + j * len(xcl)]) for i in range(len(xcl)) for j in range(len(ycl))]
    points = {','.join([str(len(xcl) - i - 1), str(len(ycl) - j - 1)]): z for i, j, x, z in elevations}
    import json
    with open('/tmp/pts.json', 'w') as f:
        json.dump(points, f)
    print(grid)

if __name__ == "__main__":
    logging.info("Precaching")
    write_raster(get_cached_50m())
