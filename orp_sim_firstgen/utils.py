# http://gis.stackexchange.com/questions/32418/python-script-to-convert-lat-long-to-itm-irish-transverse-mercator

import os
from pyproj import Proj

class DummyGridConverter:  # Class for converting latitude and longitude values into Irish Grid values
    def __call__(self, a, b, inverse=True):
        if inverse:
            return b, a
        else:
            return b, a

class GridConverter:  # Class for converting latitude and longitude values into Irish Grid values
    code = 'epsg:29903'

    def __init__(self):
        self._proj = Proj(self.code)

    def __call__(self, a, b, inverse=True):
        if inverse: # if inverse is set to true
            x, y = a, b
            lo, la = self._proj(x, y, inverse=True)
            c, d = la, lo
        else:
            lo, la = b, a
            x, y = self._proj(lo, la, inverse=False)
            c, d = x, y

        return c, d

class IrishGridConverter(GridConverter):
    pass

def in_box(a, b, x1, y1, x2, y2):
    return (a > x1 and b > y1 and a < x2 and b < y2)

REGION = os.environ.get('DATA_REGION', 'ni-1')

PRE_CONVERTERS = {
    'ni-1': IrishGridConverter, # NI
    'sa-1': DummyGridConverter, # Riyadh
    'sa-2': DummyGridConverter, # Dammam
    'tv-1': DummyGridConverter # Dammam
}
CONVERTERS = {
    'ni-1': DummyGridConverter, # NI
    'sa-1': DummyGridConverter, # Riyadh
    'sa-2': DummyGridConverter, # Dammam
    'tv-1': DummyGridConverter # Dammam
}
def get_converter():
    return CONVERTERS[REGION]

def get_pre_converter():
    return PRE_CONVERTERS[REGION]
